package me.pixka.hrgateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class HrgatewayApplication

fun main(args: Array<String>) {
	runApplication<HrgatewayApplication>(*args)
}
